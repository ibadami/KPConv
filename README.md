#Installation and run instruction 

## Dataset
- To download the dataset, create an account in [ShapeNet](www.shapenet.org).
- From "Downloads" tab, click on PartNet. Fill the form as instructed on the page and download sem_seg_h5.zip.
- Unzip the file and save to `<your_dir>/Datasets/PartNet/sem_seg_h5/` desired location.
- Download "stats" folder from [this](https://github.com/daerduoCarey/partnet_dataset) github page.
- Save to `<your_dir>/Datasets/PartNet/stats`.

## Docker image build
- Change directory to KPConv `cd KPConv`.
- Build the docker image by running `docker build -t  kpconv:latest .`
- Once built, run the docker image with this command:
`docker run -it --rm --gpus all -e DISPLAY=${DISPLAY}  --net=host -v <your_dir>/Datasets/PartNet:/Datasets/PartNet -v <path_to_KPConv>/KPConv:/KPConv --user "$(id -u):$(id -g)"  kpconv:latest`
- This will start an interactive docker session. You can run the IDE of your choice to edit the code in the docker by mounting the IDE path in the docker image using the `-v` option.
- **Note**: If you get x11 server error, run `xhost +"local:docker@"` in the terminal before running the command above for running the docker image.
  
## Training
- To run the training in the docker session:
`python3 training_PartNet.py "Chair"`
- The model and training loss, accuracy and time will be logged in the `KPConv/results` folder.

## Plotting training results
- To visualise the training progress:
`python3 plot_convergence <name of the log folder1> <name of the log folder2> "Chair"`.
- First argument is folder of the start log, second argument is folder of the end log and third is object name.
- The script can compare two training instances of the same dataset to help pick the better hyperparameters.
- If you just want to check a single training progress, pass same folder name as start and end.
- The above command does not need to be run from the docker session. As the code folders are synced between the host and the docker image, the result folder is available at location used from the host machine.

## Testing
- To test the saved model in the docker session:
`python3 test_any_model.py <name of the log folder>`
- The test results, along with best and worst segmented cloud examples will be saved under the `/KPConv/test_results` folder.

## Visualizing point clouds
- To visualize the saved point cloud examples on the local machine
`python3 visualize_point_clouds.py "/test_results/Chair/Chair/best_01.ply"`
- This will plot an interactive 3D cloud.

## Running jupyter notebook 
- On your host machine from the KPConv folder run:
`jupyter Demo.ipynb`.

---

**Note**: Apart from training and testing, rest of the code can be run from the local machine. 
