# Dockerfile-gpu
FROM tensorflow/tensorflow:1.15.2-gpu-py3

# Install OpenJDK-8
RUN apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y ant && \
    apt-get clean;

# Fix certificate issues
RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

# Setup JAVA_HOME -- useful for docker commandline
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME

# It looks for .so file hence the linking is required.
RUN cd /usr/local/lib/python3.6/dist-packages/tensorflow_core/ && ln -s ./libtensorflow_framework.so.1 ./libtensorflow_framework.so

RUN mkdir KPConv
WORKDIR /KPConv

COPY . .

# compile C++ code and custom tf operations
RUN cd ./cpp_wrappers && sh ./compile_wrappers.sh
RUN cd ./tf_custom_ops && sh ./compile_op.sh

# install other requirements
RUN pip install --upgrade pip
RUN pip install -r requirements.txt