#
#
#      0==================================0
#      |    Kernel Point Convolutions     |
#      0==================================0
#
#
# ----------------------------------------------------------------------------------------------------------------------
#
#      Class handling PartNet dataset
#
# ----------------------------------------------------------------------------------------------------------------------
#
#      Ishrat Badami - 21-07-2020
#
# ----------------------------------------------------------------------------------------------------------------------
#
#           Imports and global variables
#       \**********************************/
#

# Basic libs
import fnmatch

import tensorflow as tf
import numpy as np
import time
import pickle
import json
import h5py

# PLY reader
from utils.ply import read_ply, write_ply

# OS functions
from os import listdir, makedirs
from os.path import exists, join, dirname

# Dataset parent class
from datasets.common import Dataset

import cpp_wrappers.cpp_subsampling.grid_subsampling as cpp_subsampling


# ----------------------------------------------------------------------------------------------------------------------
#
#           Utility functions
#       \***********************/
#


def grid_subsampling(points, features=None, labels=None, sampleDl=0.1, verbose=0):
    """
    CPP wrapper for a grid subsampling (method = barycenter for points and features
    :param points: (N, 3) matrix of input points
    :param features: optional (N, d) matrix of features (floating number)
    :param labels: optional (N,) matrix of integer labels
    :param sampleDl: parameter defining the size of grid voxels
    :param verbose: 1 to display
    :return: subsampled points, with features and/or labels depending of the input
    """

    if (features is None) and (labels is None):
        return cpp_subsampling.compute(points, sampleDl=sampleDl, verbose=verbose)
    elif (labels is None):
        return cpp_subsampling.compute(points, features=features, sampleDl=sampleDl, verbose=verbose)
    elif (features is None):
        return cpp_subsampling.compute(points, classes=labels, sampleDl=sampleDl, verbose=verbose)
    else:
        return cpp_subsampling.compute(points, features=features, classes=labels, sampleDl=sampleDl, verbose=verbose)


# ----------------------------------------------------------------------------------------------------------------------
#
#           Class Definition
#       \**********************/
#


class PartNetDataset(Dataset):
    """
    PartNet dataset for segmentation task. Can handle both unique object class or multi classes models.
    """

    # Initiation methods
    # ------------------------------------------------------------------------------------------------------------------

    def __init__(self, class_name, level, input_threads=8):
        """
        Initiation method. Give the name of the object class to segment (for example 'Airplane') or 'multi' to segment
        all objects with a single model.
        """
        Dataset.__init__(self, 'PartNet_' + class_name)

        ##########################
        # Parameters for the files
        ##########################

        # Path of the folder containing ply files
        self.root = '/Datasets/PartNet/'
        self.path = '/Datasets/PartNet/sem_seg_h5/'

        # Type of dataset (one of the class names or 'multi')
        self.PartNetType = class_name
        self.level = level

        ###########################
        # Object classes parameters
        ###########################

        # Dict from object labels to names
        self.object_label_to_names = {0: 'Bag',
                               1: 'Bed',
                               2: 'Bottle',
                               3: 'Bowl',
                               4: 'Chair',
                               5: 'Clock',
                               6: 'Dishwasher',
                               7: 'Display',
                               8: 'Door',
                               9: 'Earphone',
                               10: 'Faucet',
                               11: 'Hat',
                               12: 'Keyboard',
                               13: 'Knife',
                               14: 'Lamp',
                               15: 'Laptop',
                               16: 'Microwave',
                               17: 'Mug',
                               18: 'Refrigerator',
                               19: 'Scissors',
                               20: 'StorageFurniture',
                               21: 'Table',
                               22: 'TrashCan',
                               23: 'Vase'
                               }

        self.init_object_labels()

        # List of classes ignored during training (can be empty)
        self.ignored_labels = np.array([])

        if self.PartNetType == 'multi':

            # Number of parts for each object
            self.num_parts_level_1 = [3, 3, 5, 3, 5, 5, 2, 2, 2, 5, 7, 5, 2, 4, 17, 2, 2, 3, 2, 2, 6, 10, 4, 3]
            self.num_parts_level_2 = [3, 9, 5, 3, 29, 5, 4, 2, 3, 5, 7, 5, 2, 4, 27, 2, 4, 3, 5, 2, 18, 41, 4, 3]
            self.num_parts_level_3 = [3, 14, 8, 3, 38, 10, 6, 3, 4, 9, 11, 5, 2, 9, 40, 2, 5, 3, 6, 2, 23, 50, 10, 5]

            # Number of models
            self.network_model = 'multi_segmentation'
            self.num_train = 18670
            self.num_val = 2667
            self.num_test = 5334
            self.label_to_names = self.object_label_to_names
            self.init_labels()

        elif self.PartNetType in self.object_label_names:

            # Number of models computed when init_subsample_clouds is called
            self.network_model = 'segmentation'
            # Load current category + level statistics
            self.label_to_names = {0:"other"}

            with open(join(self.root, 'stats/after_merging_label_ids/%s-level-%d.txt' % (self.PartNetType, self.level)), 'r')as fin:
                lines = fin.readlines()
                self.num_parts = len(lines) + 1  # with "others"
                for i, line in enumerate(lines):
                    self.label_to_names[i+1] = line.split(' ')[1].split('/')[-2]+'/'+line.split(' ')[1].split('/')[-1]
            self.init_labels()

            self.label_weights_list = [1.0] * self.num_parts

        else:
            raise ValueError('Unsupported PartNet object class : \'{:s}\''.format(self.PartNetType))

        # Number of threads
        self.num_threads = input_threads

        # Initiate containers
        self.input_points = {'train': [], 'val': [], 'test': []}
        self.input_labels = {'train': [], 'val': [], 'test': []}
        self.input_point_labels = {'train': [], 'val': [], 'test': []}
        return

    # Utility methods
    # ------------------------------------------------------------------------------------------------------------------

    def get_batch_gen(self, split, config):

        ################
        # Def generators
        ################

        # Initiate potentials for regular generation
        if not hasattr(self, 'potentials'):
            self.potentials = {}

        # Reset potentials
        self.potentials[split] = np.random.rand(len(self.input_labels[split])) * 1e-3

        def variable_batch_gen_multi():

            # Initiate concatenation lists
            tp_list = []
            tl_list = []
            tpl_list = []
            ti_list = []
            batch_n = 0

            # Initiate parameters depending on the chosen split
            if split == 'train':
                gen_indices = np.random.permutation(self.num_train)

            elif split == 'val':

                # Get indices with the minimum potential
                val_num = min(self.num_test, config.validation_size * config.batch_num)
                if val_num < self.potentials[split].shape[0]:
                    gen_indices = np.argpartition(self.potentials[split], val_num)[:val_num]
                else:
                    gen_indices = np.random.permutation(val_num)

                # Update potentials
                self.potentials[split][gen_indices] += 1.0

            elif split == 'test':

                # Get indices with the minimum potential
                val_num = min(self.num_test, config.validation_size * config.batch_num)
                if val_num < self.potentials[split].shape[0]:
                    gen_indices = np.argpartition(self.potentials[split], val_num)[:val_num]
                else:
                    gen_indices = np.random.permutation(val_num)

                # Update potentials
                self.potentials[split][gen_indices] += 1.0

            else:
                raise ValueError('Split argument in data generator should be "training", "validation" or "test"')

            # Generator loop
            for i, rand_i in enumerate(gen_indices):

                # Get points
                new_points = self.input_points[split][rand_i].astype(np.float32)
                n = new_points.shape[0]

                # In case batch is full, yield it and reset it
                if batch_n + n > self.batch_limit:
                    yield (np.concatenate(tp_list, axis=0),
                           np.array(tl_list, dtype=np.int32),
                           np.concatenate(tpl_list, axis=0),
                           np.array(ti_list, dtype=np.int32),
                           np.array([tp.shape[0] for tp in tp_list]))
                    tp_list = []
                    tl_list = []
                    tpl_list = []
                    ti_list = []
                    batch_n = 0

                # Add data to current batch
                tp_list += [new_points]
                tl_list += [self.input_labels[split][rand_i]]
                tpl_list += [np.squeeze(self.input_point_labels[split][rand_i])]
                ti_list += [rand_i]

                # Update batch size
                batch_n += n

            yield (np.concatenate(tp_list, axis=0),
                   np.array(tl_list, dtype=np.int32),
                   np.concatenate(tpl_list, axis=0),
                   np.array(ti_list, dtype=np.int32),
                   np.array([tp.shape[0] for tp in tp_list]))

        def variable_batch_gen_segment():

            # Initiate concatenation lists
            tp_list = []
            tpl_list = []
            ti_list = []
            batch_n = 0

            # Initiate parameters depending on the chosen split
            if split == 'train':
                gen_indices = np.random.permutation(self.num_train)
            elif split == 'val':
                gen_indices = np.random.permutation(self.num_val)
            elif split == 'test':
                gen_indices = np.arange(self.num_test)
            else:
                raise ValueError('Split argument in data generator should be "training", "validation" or "test"')

            # Generator loop
            for i, rand_i in enumerate(gen_indices):

                # Get points
                new_points = self.input_points[split][rand_i].astype(np.float32)
                n = new_points.shape[0]

                # In case batch is full, yield it and reset it
                if batch_n + n > self.batch_limit:
                    yield (np.concatenate(tp_list, axis=0),
                           np.concatenate(tpl_list, axis=0),
                           np.array(ti_list, dtype=np.int32),
                           np.array([tp.shape[0] for tp in tp_list]))
                    tp_list = []
                    tpl_list = []
                    ti_list = []
                    batch_n = 0

                # Add data to current batch
                tp_list += [new_points]
                tpl_list += [np.squeeze(self.input_point_labels[split][rand_i])]
                ti_list += [rand_i]

                # Update batch size
                batch_n += n

            yield (np.concatenate(tp_list, axis=0),
                   np.concatenate(tpl_list, axis=0),
                   np.array(ti_list, dtype=np.int32),
                   np.array([tp.shape[0] for tp in tp_list]))

        ###################
        # Choose generators
        ###################

        if self.PartNetType == 'multi':
            # Generator types and shapes
            gen_types = (tf.float32, tf.int32, tf.int32, tf.int32, tf.int32)
            gen_shapes = ([None, 3], [None], [None], [None], [None])
            return variable_batch_gen_multi, gen_types, gen_shapes

        elif self.PartNetType in self.object_label_names:

            # Generator types and shapes
            gen_types = (tf.float32, tf.int32, tf.int32, tf.int32)
            gen_shapes = ([None, 3], [None], [None], [None])
            return variable_batch_gen_segment, gen_types, gen_shapes
        else:
            raise ValueError('Unsupported PartNet dataset type')

    def get_tf_mapping(self, config):

        def tf_map_multi(stacked_points, object_labels, point_labels, obj_inds, stack_lengths):
            """
            From the input point cloud, this function computes all the point clouds at each layer, the neighbors
            indices, the pooling indices and other useful variables.
            :param stacked_points: Tensor with size [None, 3] where None is the total number of points
            :param labels: Tensor with size [None] where None is the number of batch
            :param stack_lengths: Tensor with size [None] where None is the number of batch
            """

            # Get batch indices for each point
            batch_inds = self.tf_get_batch_inds(stack_lengths)

            # Augment input points
            stacked_points, scales, rots = self.tf_augment_input(stacked_points,
                                                                 batch_inds,
                                                                 config)

            # First add a column of 1 as feature for the network to be able to learn 3D shapes
            stacked_features = tf.ones((tf.shape(stacked_points)[0], 1), dtype=tf.float32)

            # Then use positions or not
            if config.in_features_dim == 1:
                pass
            elif config.in_features_dim == 4:
                stacked_features = tf.concat((stacked_features, stacked_points), axis=1)
            elif config.in_features_dim == 7:
                stacked_features = tf.concat((stacked_features, stacked_points, tf.square(stacked_points)), axis=1)
            else:
                raise ValueError('Only accepted input dimensions are 1, 4 and 7 (without and with XYZ)')

            # Get the whole input list
            input_list = self.tf_segmentation_inputs(config,
                                                     stacked_points,
                                                     stacked_features,
                                                     point_labels,
                                                     stack_lengths,
                                                     batch_inds,
                                                     object_labels=object_labels)

            # Add scale and rotation for testing
            input_list += [scales, rots, obj_inds]

            return input_list

        def tf_map_segment(stacked_points, point_labels, obj_inds, stack_lengths):
            """
            From the input point cloud, this function compute all the point clouds at each layer, the neighbors
            indices, the pooling indices and other useful variables.
            :param stacked_points: Tensor with size [None, 3] where None is the total number of points
            :param point_labels: Tensor with size [None] where None is the number of batch
            :param obj_inds:
            :param stack_lengths: Tensor with size [None] where None is the number of batch
            """

            # Get batch indices for each point
            batch_inds = self.tf_get_batch_inds(stack_lengths)

            # Augment input points
            stacked_points, scales, rots = self.tf_augment_input(stacked_points,
                                                                 batch_inds,
                                                                 config)

            # First add a column of 1 as feature for the network to be able to learn 3D shapes
            stacked_features = tf.ones((tf.shape(stacked_points)[0], 1), dtype=tf.float32)

            # Then use positions or not
            if config.in_features_dim == 1:
                pass
            elif config.in_features_dim == 4:
                stacked_features = tf.concat((stacked_features, stacked_points), axis=1)
            elif config.in_features_dim == 7:
                stacked_features = tf.concat((stacked_features, stacked_points, tf.square(stacked_points)), axis=1)
            else:
                raise ValueError('Only accepted input dimensions are 1, 4 and 7 (without and with XYZ)')

            # Get the whole input list
            input_list = self.tf_segmentation_inputs(config,
                                                     stacked_points,
                                                     stacked_features,
                                                     point_labels,
                                                     stack_lengths,
                                                     batch_inds)

            # Add scale and rotation for testing
            input_list += [scales, rots, obj_inds]

            return input_list

        if self.PartNetType == 'multi':
            return tf_map_multi

        elif self.PartNetType in self.object_label_names:
            return tf_map_segment
        else:
            raise ValueError('Unsupported PartNet dataset type')

    # Debug methods
    # ------------------------------------------------------------------------------------------------------------------

    def check_input_pipeline_timing(self, config):

        # Create a session for running Ops on the Graph.
        cProto = tf.ConfigProto()
        cProto.gpu_options.allow_growth = True
        self.sess = tf.Session(config=cProto)

        # Init variables
        self.sess.run(tf.global_variables_initializer())

        # Initialise iterator with train data
        self.sess.run(self.train_init_op)

        # Run some epochs
        t0 = time.time()
        mean_dt = np.zeros(2)
        last_display = t0
        epoch = 0
        training_step = 0
        while epoch < 100:

            try:
                # Run one step of the model.
                t = [time.time()]
                ops = self.flat_inputs

                # Get next inputs
                np_flat_inputs = self.sess.run(ops)
                t += [time.time()]

                # Restructure flatten inputs
                points = np_flat_inputs[:config.num_layers]
                neighbors = np_flat_inputs[config.num_layers:2 * config.num_layers]
                t += [time.time()]

                # Average timing
                mean_dt = 0.99 * mean_dt + 0.01 * (np.array(t[1:]) - np.array(t[:-1]))

                # Console display
                if (t[-1] - last_display) > 1.0 / 10:
                    last_display = t[-1]
                    message = 'Step {:08d} : timings {:4.2f} {:4.2f} - {:d} x {:d}'
                    print(message.format(training_step,
                                         1000 * mean_dt[0],
                                         1000 * mean_dt[1],
                                         neighbors[0].shape[0],
                                         neighbors[0].shape[1]))

                training_step += 1

            except tf.errors.OutOfRangeError:
                print('End of train dataset')
                self.sess.run(self.train_init_op)
                epoch += 1

        return

    def check_input_pipeline_2(self, config):

        # Create a session for running Ops on the Graph.
        cProto = tf.ConfigProto()
        cProto.gpu_options.allow_growth = True
        self.sess = tf.Session(config=cProto)

        # Init variables
        self.sess.run(tf.global_variables_initializer())

        # Initialise iterator with train data
        self.sess.run(self.train_init_op)

        # Run some epochs
        epoch = 0
        while epoch < 100:

            try:
                # Run one step of the model.
                t = [time.time()]

                # Get next inputs
                np_flat_inputs_1 = self.sess.run(self.flat_inputs)

            except tf.errors.OutOfRangeError:
                print('End of train dataset')
                self.sess.run(self.train_init_op)
                epoch += 1
        return

    def check_debug_input(self, config, path):

        # Get debug file
        file = join(path, 'all_debug_inputs.pkl')
        with open(file, 'rb') as f1:
            inputs = pickle.load(f1)

        # Print inputs
        nl = config.num_layers
        for layer in range(nl):
            print('Layer : {:d}'.format(layer))

            points = inputs[layer]
            neighbors = inputs[nl + layer]
            pools = inputs[2 * nl + layer]
            upsamples = inputs[3 * nl + layer]

            nan_percentage = 100 * np.sum(np.isnan(points)) / np.prod(points.shape)
            print('Points =>', points.shape, '{:.1f}% NaN'.format(nan_percentage))
            nan_percentage = 100 * np.sum(np.isnan(neighbors)) / np.prod(neighbors.shape)
            print('neighbors =>', neighbors.shape, '{:.1f}% NaN'.format(nan_percentage))
            nan_percentage = 100 * np.sum(np.isnan(pools)) / (np.prod(pools.shape) + 1e-6)
            print('pools =>', pools.shape, '{:.1f}% NaN'.format(nan_percentage))
            nan_percentage = 100 * np.sum(np.isnan(upsamples)) / (np.prod(upsamples.shape) + 1e-6)
            print('upsamples =>', upsamples.shape, '{:.1f}% NaN'.format(nan_percentage))

        ind = 4 * nl
        features = inputs[ind]
        nan_percentage = 100 * np.sum(np.isnan(features)) / np.prod(features.shape)
        print('features =>', features.shape, '{:.1f}% NaN'.format(nan_percentage))
        ind += 1
        batch_weights = inputs[ind]
        ind += 1
        in_batches = inputs[ind]
        max_b = np.max(in_batches)
        print(in_batches.shape)
        in_b_sizes = np.sum(in_batches < max_b - 0.5, axis=-1)
        print('in_batch_sizes =>', in_b_sizes)
        ind += 1
        out_batches = inputs[ind]
        max_b = np.max(out_batches)
        print(out_batches.shape)
        out_b_sizes = np.sum(out_batches < max_b - 0.5, axis=-1)
        print('out_batch_sizes =>', out_b_sizes)
        ind += 1
        point_labels = inputs[ind]
        ind += 1
        if config.dataset.startswith('PartNet_multi'):
            object_labels = inputs[ind]
            nan_percentage = 100 * np.sum(np.isnan(object_labels)) / np.prod(object_labels.shape)
            print('object_labels =>', object_labels.shape, '{:.1f}% NaN'.format(nan_percentage))
            ind += 1
        augment_scales = inputs[ind]
        ind += 1
        augment_rotations = inputs[ind]
        ind += 1

        print('\npoolings and upsamples nums :\n')

        # Print inputs
        nl = config.num_layers
        for layer in range(nl):

            print('\nLayer : {:d}'.format(layer))

            neighbors = inputs[nl + layer]
            pools = inputs[2 * nl + layer]
            upsamples = inputs[3 * nl + layer]

            max_n = np.max(neighbors)
            nums = np.sum(neighbors < max_n - 0.5, axis=-1)
            print('min neighbors =>', np.min(nums))

            if np.prod(pools.shape) > 0:
                max_n = np.max(pools)
                nums = np.sum(pools < max_n - 0.5, axis=-1)
                print('min pools =>', np.min(nums))

            if np.prod(upsamples.shape) > 0:
                max_n = np.max(upsamples)
                nums = np.sum(upsamples < max_n - 0.5, axis=-1)
                print('min upsamples =>', np.min(nums))

        print('\nFinished\n\n')
        time.sleep(0.5)

        self.flat_inputs = [tf.Variable(in_np, trainable=False) for in_np in inputs]

    def readHDF5(self, split):

        if self.PartNetType == 'multi':
            print("All class training is not supported yet")
        else:
            split_file_name = split+'_files.txt'
            filelist = join(self.path, '%s-%d' % (self.PartNetType, self.level), split_file_name)

            folder = dirname(filelist)
            for line in open(filelist):
                data = h5py.File(join(folder, line.strip()))
                p = data['data'][...].astype(np.float32)
                list_p = [p[i] for i in range(np.shape(p)[0])]
                self.input_points[split].extend(list_p)

                l = data['label_seg'][...].astype(np.float32)
                list_l = [l[i] for i in range(np.shape(l)[0])]
                self.input_point_labels[split].extend(list_l)

            self.input_labels[split] = np.tile(self.input_labels[split], len(data))

            data = self.input_points[split]
            model_label = self.input_labels[split]
            point_labels = self.input_point_labels[split]

            return data, model_label, point_labels
