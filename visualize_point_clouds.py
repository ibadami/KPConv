import sys

import numpy as np
from utils.ply import read_ply
import open3d
from skimage.color.colorlabel import label2rgb
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d


def draw_point_cloud(filename, colormap):

    # give path to ".npy" file
    data = read_ply(filename)

    xyz = np.array([data['x'], data['y'], data['z']]).T
    rgb_gt = label2rgb(label=data['gt'], image=np.ones(data['gt'].shape), colors=colormap)
    rgb_pre = label2rgb(label=data['pre'], image=np.ones(data['pre'].shape), colors=colormap)

    fig = plt.figure(figsize=(25, 12.5))

    ax = fig.add_subplot(1, 2, 1, projection='3d')
    ax.scatter(xyz[:, 0], -xyz[:, 2], xyz[:, 1], c=rgb_gt, s=10)

    ax = fig.add_subplot(1, 2, 2, projection='3d')
    ax.scatter(xyz[:, 0], -xyz[:, 2], xyz[:, 1], c=rgb_pre, s=10)

    plt.show()


if __name__ == '__main__':

    args = sys.argv
    colors = np.array([[0.75553866, 0.80641781, 0.09500362],
       [0.92448894, 0.13747458, 0.19353599],
       [0.10890166, 0.27787824, 0.12464292],
       [0.61590566, 0.06419033, 0.38295323],
       [0.21064764, 0.73563156, 0.67154956],
       [0.77192562, 0.21254096, 0.77257674],
       [0.11768091, 0.31598658, 0.03137352],
       [0.78765666, 0.35335663, 0.52194528],
       [0.94647724, 0.27282335, 0.16870354],
       [0.20912046, 0.55717927, 0.79090633]])
    filename = args[1]
    draw_point_cloud(filename, colors)
